const { user } = require('../models/user');
const createUserValid = (req, res, next) => {
    // TODO: Implement validatior for user entity during creation
    const validKeys = Object.keys(user).filter(key => key !== 'id');
    const allFieldAvailable = validKeys.every(key => req.body.hasOwnProperty(key));
    if (
        !allKeysValid(req.body) ||
        !allFieldAvailable ||
        !isValidEmail(req.body.email) ||
        !isValidPhone(req.body.phoneNumber) ||
        !isValidPassword(req.body.password)
    ) {
        req.body = { error: true, message: 'Invalid user data' };
    }
    next();
};

const updateUserValid = (req, res, next) => {
    // TODO: Implement validatior for user entity during update
    if (
        !allKeysValid(req.body) ||
        (req.body.email && !isValidEmail(req.body.email)) ||
        (req.body.phoneNumber && !isValidPhone(req.body.phoneNumber)) ||
        (req.body.password && !isValidPassword(req.body.password))
    ) {
        req.body = { error: true, message: 'Invalid edit data.' };
    }
    next();
};

const isValidEmail = email => email.endsWith('@gmail.com');
const isValidPhone = phone => phone.startsWith('+380') && phone.length === 13;
const isValidPassword = pswd => pswd.length >= 3;
const allKeysValid = userObject => {
    const validKeys = Object.keys(user).filter(key => key !== 'id');
    return Object.keys(userObject).every(key => validKeys.includes(key));
};

exports.createUserValid = createUserValid;
exports.updateUserValid = updateUserValid;
