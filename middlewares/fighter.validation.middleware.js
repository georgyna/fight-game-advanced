const { fighter } = require('../models/fighter');

const createFighterValid = (req, res, next) => {
    // TODO: Implement validatior for fighter entity during creation
    const validKeys = Object.keys(fighter).filter(key => key !== 'id');
    if (!req.body.hasOwnProperty('health')) { req.body.health = fighter.health; }
    const allFieldAvailable = validKeys.every(key => req.body.hasOwnProperty(key));
    if (
        !allKeysValid(req.body) ||
        !allFieldAvailable ||
        !isValidPower(req.body.power) ||
        !isValidDefense(req.body.defense)
    ) {
        req.body = { error: true, message: 'Invalid fighter data.' };
    }
    next();
}

const updateFighterValid = (req, res, next) => {
    // TODO: Implement validatior for fighter entity during update
    if (!allKeysValid(req.body)) {
        req.body = { error: true, message: 'Invalid edit data.' };
    }
    next();
}

const isNumber = value => typeof value === 'number';
const isValidPower = power => isNumber(power) && power > 0 && power < 100;
const isValidDefense = defense => isNumber(defense) && defense >= 1 && defense <= 10;
const allKeysValid = fighterObject => {
    const validKeys = Object.keys(fighter).filter(key => key !== 'id');
    return Object.keys(fighterObject).every(key => validKeys.includes(key));
}

exports.createFighterValid = createFighterValid;
exports.updateFighterValid = updateFighterValid;