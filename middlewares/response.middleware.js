const responseMiddleware = (req, res, next) => {
   // TODO: Implement middleware that returns result of the query
    if (req.body.error && !req.body.status) {
        res.status(400).json({
            error: true,
            message: req.body.message
        })
    } else if (req.body.status && req.body.status === 'ok') {
        res.status(200).json(req.body.data);
    } else if (!req.route || (req.body.error && req.body.status === 'notfound')) {
        res.status(404).json({
            error: true,
            message: 'Source not found.'
        })
    }
};

exports.responseMiddleware = responseMiddleware;