const { Router } = require('express');
const UserService = require('../services/userService');
const { createUserValid, updateUserValid } = require('../middlewares/user.validation.middleware');
const { responseMiddleware } = require('../middlewares/response.middleware');

const router = Router();

// TODO: Implement route controllers for user
router.get('/', function (req, res, next) {
    try {
        const users = UserService.getUsers();
        req.body = { status: 'ok', data: users };
    } catch (err) {
        req.body = { error: true, message: 'Unable to get users.' };
    } finally {
        next();
    }
});

router.get('/:id', function (req, res, next) {
    try {
        const user = UserService.search(req.params);
        req.body = { status: 'ok', data: user };
    } catch (err) {
        req.body = { error: true, message: `Unable to get user with id=${req.params.id}.` };
    } finally {
        next();
    }
});

router.post('/', createUserValid, function (req, res, next) {
    try {
        if (!req.body.error) {
            const user = { ...req.body };
            const createdUser = UserService.addUser(user);
            req.body = { status: 'ok', data: createdUser };
        }
    } catch (err) {
        req.body = { error: true, message: 'Unable to add new user.' };
    } finally {
        next();
    }
});

router.put('/:id', updateUserValid, function (req, res, next) {
    try {
        if (!req.body.error) {
            const user = { ...req.body };
            const editedUser = UserService.editUser(req.params, user);
            req.body = editedUser ? { status: 'ok', data: editedUser } : { error: true, message: 'User not exists.' };
        }
    } catch (err) {
        req.body = { error: true, message: 'Unable to edit user information.' };
    } finally {
        next();
    }
});

router.delete('/:id', function (req, res, next) {
    try {
        const [deletedUser] = UserService.deleteUser(req.params);
        req.body = deletedUser
            ? { status: 'ok', data: { message: `User with id=${req.params.id} was deleted successfully` } }
            : { error: true, status: 'notfound' };
    } catch (err) {
        req.body = { error: true, message: 'Unable to delete user.' };
    } finally {
        next();
    }
});

router.use(responseMiddleware);

module.exports = router;
