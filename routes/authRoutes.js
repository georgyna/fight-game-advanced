const { Router } = require('express');
const AuthService = require('../services/authService');
const { responseMiddleware } = require('../middlewares/response.middleware');

const router = Router();

router.post('/login', (req, res, next) => {
    try {
        // TODO: Implement login action (get the user if it exist with entered credentials)
        const userData = { ...req.body };
        const authorizedUser = AuthService.login(userData);
        req.body = { status: 'ok', data: authorizedUser };
    } catch (err) {
        req.body = { error: true, message: 'Unable to login.' };
    } finally {
        next();
    }
}, responseMiddleware);

module.exports = router;