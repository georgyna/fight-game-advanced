const { Router } = require('express');
const FighterService = require('../services/fighterService');
const { responseMiddleware } = require('../middlewares/response.middleware');
const { createFighterValid, updateFighterValid } = require('../middlewares/fighter.validation.middleware');

const router = Router();

// TODO: Implement route controllers for fighter
router.get('/', function (req, res, next) {
    try {
        const fighters = FighterService.getFighters();
        req.body = { status: 'ok', data: fighters };
    } catch (err) {
        req.body = { error: true, message: 'Unable to get fighters.' };
    } finally {
        next();
    }
});

router.get('/:id', function (req, res, next) {
    try {
        const fighter = FighterService.search(req.params);
        req.body = { status: 'ok', data: fighter };
    } catch (err) {
        req.body = { error: true, message: `Unable to get fighter with id=${req.params.id}.` };
    } finally {
        next();
    }
});

router.post('/', createFighterValid, function (req, res, next) {
    try {
        if (!req.body.error) {
            const fighter = { ...req.body };
            const newFighter = FighterService.addFighter(fighter);
            req.body = { status: 'ok', data: newFighter };
        }
    } catch (err) {
        req.body = { error: true, message: 'Unable to add new fighter.' };
    } finally {
        next();
    }
});

router.put('/:id', updateFighterValid, function (req, res, next) {
    try {
        if (!req.body.error) {
            const fighter = { ...req.body };
            const updatedFighter = FighterService.editFighter(req.params, fighter);
            req.body = { status: 'ok', data: updatedFighter };
        }
    } catch (err) {
        req.body = { error: true, message: 'Unable to edit fighter information.' };
    } finally {
        next();
    }
});

router.delete('/:id', function (req, res, next) {
    try {
        const [deletedFighter] = FighterService.deleteFighter(req.params);
        req.body = deletedFighter
            ? { status: 'ok', data: { message: `Fighter with id=${req.params.id} was deleted successfully` } }
            : { error: true, status: 'notfound' };
    } catch (err) {
        req.body = { error: true, message: 'Unable to delete fighter.' };
    } finally {
        next();
    }
});

router.use(responseMiddleware);

module.exports = router;
