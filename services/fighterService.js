const { FighterRepository } = require('../repositories/fighterRepository');

class FighterService {
    // TODO: Implement methods to work with fighters
    search(search) {
        const item = FighterRepository.getOne(search);
        if(!item) {
            return null;
        }
        return item;
    }

    getFighters() {
        return FighterRepository.getAll();
    }

    addFighter(fighter) {
        return FighterRepository.create(fighter);
    }

    editFighter({ id }, fighterData) {
        return FighterRepository.update(id, fighterData);
    }

    deleteFighter({ id }) {
        return FighterRepository.delete(id);
    }
}

module.exports = new FighterService();