const { UserRepository } = require('../repositories/userRepository');

class UserService {

    // TODO: Implement methods to work with user

    search(search) {
        const item = UserRepository.getOne(search);
        if(!item) {
            return null;
        }
        return item;
    }

    getUsers() {
        return UserRepository.getAll();
    }

    addUser(user) {
        return UserRepository.create(user);
    }

    editUser({ id }, userData) {
        return this.search({ id: id }) ? UserRepository.update(id, userData) : null;
    }

    deleteUser({ id }) {
        return UserRepository.delete(id);
    }
}

module.exports = new UserService();